<?php

namespace Prima\CMS\Application\Service;

use Prima\CMS\Domain\Model\DTO;

class DTOSerializer
{
    /**
     * @param DTO[] | DTO $objects
     * @return array
     */
    public static function serialize($objects)
    {
        if (is_array($objects)) {
            $result = static::serializeDTOs($objects);
        } else {
            $result = $objects->serialize();
        }

        return $result;
    }

    /**
     * @param DTO[] $dtos
     * @return array
     */
    private static function serializeDTOs($dtos)
    {
        $result = array_map(function (DTO $dto) {
            return $dto->serialize();
        }, $dtos);

        return $result;
    }
}
