<?php

namespace Prima\CMS\Application\Service\Document;

use Prima\CMS\Domain\Model\Document\Document;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;

class URLGenerator
{
    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * URLGenerator constructor.
     * @param RouterInterface $router
     */
    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    public function generate(Document $document): string
    {
        return $this->router->generate(
            'document_content',
            ['id' => $document->documentId()],
            UrlGeneratorInterface::ABSOLUTE_URL
        );
    }
}
