<?php

namespace Prima\CMS\Application\Service\Document;

use Prima\CMS\Application\Exception\FileContentsRetrievalException;
use Prima\CMS\Domain\Command\CreateDocument;
use Prima\CMS\Domain\Command\RemoveDocument;
use Prima\CMS\Domain\Exception\DocumentNotFoundException;
use Prima\CMS\Domain\Model\Document\Document;
use Prima\CMS\Domain\Model\Document\DocumentId;
use Prima\CMS\Domain\Model\Document\DocumentName;
use Prima\CMS\Domain\Model\Document\DocumentRepository;
use Prima\CMS\Domain\Model\Document\MimeType;
use Prima\CMS\Domain\Service\IdGenerator;

class DocumentService
{
    /**
     * @var DocumentRepository
     */
    private $documentRepository;
    /**
     * @var IdGenerator
     */
    private $idGenerator;

    public function __construct(DocumentRepository $documentRepository, IdGenerator $idGenerator)
    {
        $this->documentRepository = $documentRepository;
        $this->idGenerator = $idGenerator;
    }

    public function createDocument(CreateDocument $createDocument): Document
    {
        $uploadedFile = $createDocument->uploadedFile();
        $fileName = $uploadedFile->getClientOriginalName();

        if (!$data = file_get_contents($uploadedFile)) {
            throw new FileContentsRetrievalException();
        }

        $document = Document::create(
            DocumentId::fromString($this->idGenerator->next()),
            DocumentName::fromString($fileName),
            MimeType::fromString($uploadedFile->getClientMimeType()),
            $data
        );

        $this->documentRepository->save($document);

        return $document;
    }

    public function removeDocument(RemoveDocument $removeDocument)
    {
        if (!$document = $this->documentRepository->get(DocumentId::fromString($removeDocument->documentId()))) {
            throw new DocumentNotFoundException();
        }

        $this->documentRepository->remove($document);
    }
}
