<?php

namespace Prima\CMS\Application\Service\Document;

use Prima\CMS\Domain\Model\Document\Document;
use Prima\CMS\Domain\Model\Document\DocumentDTO;

class DocumentDTOHydrator
{
    /**
     * @var URLGenerator
     */
    private $urlGenerator;

    public function __construct(URLGenerator $urlGenerator)
    {
        $this->urlGenerator = $urlGenerator;
    }

    public function hydrate(Document $document): DocumentDTO
    {
        return new DocumentDTO(
            $document->documentId(),
            $document->name(),
            $document->mimeType(),
            $this->urlGenerator->generate($document)
        );
    }
}
