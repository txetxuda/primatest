<?php

namespace Prima\CMS\Application\Exception;

class FileContentsRetrievalException extends ApplicationException
{
}
