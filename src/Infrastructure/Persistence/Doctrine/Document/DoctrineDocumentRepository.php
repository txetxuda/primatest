<?php

namespace Prima\CMS\Infrastructure\Persistence\Doctrine\Document;

use Doctrine\ORM\EntityRepository;
use Prima\CMS\Application\Service\Document\DocumentDTOHydrator;
use Prima\CMS\Domain\Model\Document\Document;
use Prima\CMS\Domain\Model\Document\DocumentDTO;
use Prima\CMS\Domain\Model\Document\DocumentId;
use Prima\CMS\Domain\Model\Document\DocumentRepository;

class DoctrineDocumentRepository extends EntityRepository implements DocumentRepository
{
    /** @var  DocumentDTOHydrator */
    private $documentDTOHydrator;

    /**
     * @param DocumentId $documentId
     * @return null|Document
     */
    public function get(DocumentId $documentId)
    {
        return $this->find($documentId);
    }

    /**
     * @return DocumentDTO[]
     */
    public function all()
    {
        /** @var Document[] $documents */
        $documents = $this->findAll();
        $documentDTOs = [];

        foreach ($documents as $document) {
            $documentDTOs[] = $this->documentDTOHydrator->hydrate($document);
        }

        return $documentDTOs;
    }

    /**
     * @param DocumentId $documentId
     * @return null|DocumentDTO
     */
    public function documentOfId(DocumentId $documentId)
    {
        /** @var Document $document */
        if (!$document = $this->find($documentId)) {
            return null;
        }

        return $this->documentDTOHydrator->hydrate($document);
    }

    public function save(Document $document)
    {
        $this->_em->persist($document);
        $this->_em->flush();
    }

    public function remove(Document $document)
    {
        $this->_em->remove($document);
        $this->_em->flush();
    }

    public function setHydrator(DocumentDTOHydrator $documentDTOHydrator)
    {
        $this->documentDTOHydrator = $documentDTOHydrator;
    }
}
