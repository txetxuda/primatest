<?php

namespace Prima\CMS\Infrastructure\Service;

use Prima\CMS\Domain\Model\AggregateId;
use Prima\CMS\Domain\Model\Document\DocumentId;
use Prima\CMS\Domain\Service\IdGenerator;
use Ramsey\Uuid\Uuid;

class DocumentIdGenerator implements IdGenerator
{
    /**
     * @return AggregateId UUID
     */
    public function next()
    {
        return DocumentId::fromString(Uuid::uuid4());
    }
}
