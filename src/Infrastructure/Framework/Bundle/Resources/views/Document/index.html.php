<html>
    <head>
        <title>Prima CMS</title>
        <link href="node_modules/bootstrap/dist/css/bootstrap.css" rel="stylesheet" type="text/css">
        <link href="node_modules/bootstrap/dist/css/bootstrap-theme.css" rel="stylesheet" type="text/css">
        <link href="node_modules/jquery-bootgrid/dist/jquery.bootgrid.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <table id="grid-data" class="table table-condensed table-hover table-striped">
            <thead>
                <tr>
                    <th data-column-id="id" data-type="numeric">ID</th>
                    <th data-column-id="name">Name</th>
                    <th data-column-id="mime_type" data-order="desc">Mime Type</th>
                    <th data-column-id="url" data-formatter="link" data-sortable="false">Link</th>
                    <th data-column-id="actions" data-formatter="commands" data-sortable="false">Actions</th>
                </tr>
            </thead>
        </table>
        <form action="myform.cgi">
            <label class="btn btn-default btn-file">
                New document <input type="file" style="display: none;">
            </label>
            <button type="submit" class="btn btn-default">Upload</button>
        </form>
        <script src="node_modules/jquery/dist/jquery.js"></script>
        <script src="node_modules/bootstrap/dist/js/bootstrap.js"></script>
        <script src="node_modules/jquery-bootgrid/dist/jquery.bootgrid.js"></script>
        <script>
            var grid = $("#grid-data").bootgrid({
                ajaxSettings: {
                    method: "GET",
                    contentType: "application/json"
                },
                ajax: true,
                url: "/api/v1/documents",
                formatters: {
                    "link": function(column, row)
                    {
                        return "<a target='_blank' href=\"" + row.url + "\">Open document</a>";
                    },
                    "commands": function(column, row)
                    {
                        return "<div class=\"btn-group\">" +
                            "<button type=\"button\" class=\"btn btn-sm btn-default command-delete\" data-row-id=\"" + row.id + "\">" +
                            "<span class=\"glyphicon glyphicon-trash\"></span></button>" +
                            "</div>";
                    }
                },
                templates: {
                    search: ""
                }
            }).on("loaded.rs.jquery.bootgrid", function()
            {
                grid.find(".command-delete").on("click", function(e)
                {
                    deleteFile.call(this);
                });
            });

            function deleteFile() {
                $.ajax({
                    url: '/api/v1/documents/' + $(this).data("row-id"),
                    type: 'DELETE',
                    dataType: 'json',
                    processData: false,
                    contentType: false,
                    success: function (data, textStatus, jqXHR) {
                        $('#grid-data').bootgrid('reload');
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log('ERRORS: ' + textStatus);
                    }
                });
            }

        </script>
        <script>
            var files;
            $('input[type=file]').on('change', prepareUpload);

            function prepareUpload(event)
            {
                files = event.target.files;
            }

            $('form').on('submit', uploadFiles);

            function uploadFiles(event)
            {
                event.stopPropagation();
                event.preventDefault();

                var data = new FormData();
                $.each(files, function(key, value)
                {
                    data.append(key, value);
                });

                $.ajax({
                    url: '/api/v1/documents',
                    type: 'POST',
                    data: data,
                    cache: false,
                    dataType: 'json',
                    processData: false,
                    contentType: false,
                    success: function(data, textStatus, jqXHR)
                    {
                        $('#grid-data').bootgrid('reload');
                    },
                    error: function(jqXHR, textStatus, errorThrown)
                    {
                        var response = JSON.parse(jqXHR.responseText);
                        alert('ERRORS: ' + response.error);
                    }
                });
            }

        </script>
    </body>
</html>
