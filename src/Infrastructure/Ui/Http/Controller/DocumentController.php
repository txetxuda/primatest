<?php

namespace Prima\CMS\Infrastructure\Ui\Http\Controller;

use Prima\CMS\Application\Service\Document\DocumentService;
use Prima\CMS\Application\Service\DTOSerializer;
use Prima\CMS\Domain\Command\CreateDocument;
use Prima\CMS\Domain\Command\RemoveDocument;
use Prima\CMS\Domain\Model\Document\DocumentId;
use Prima\CMS\Domain\Model\Document\DocumentRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Bundle\FrameworkBundle\Templating\PhpEngine;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DocumentController extends Controller
{
    /**
     * @var DocumentRepository
     */
    private $documentRepository;
    /**
     * @var DocumentService
     */
    private $documentService;
    /**
     * @var PhpEngine
     */
    private $engine;

    public function __construct(
        DocumentService $documentService,
        DocumentRepository $documentRepository,
        PhpEngine $engine
    ) {
        $this->documentService = $documentService;
        $this->documentRepository = $documentRepository;
        $this->engine = $engine;
    }

    public function indexAction()
    {
        return $this->engine->renderResponse('PrimaCMSBundle:Document:index.html.php');
    }

    public function listAction(Request $request): JsonResponse
    {
        $documents = $this->documentRepository->all();

        if (count($documents) === 0) {
            return new JsonResponse([
                'current' => 1,
                'rowCount' => 0,
                'rows' => [],
                'total' => 0,
            ]);
        }

        return new JsonResponse([
            'current' => 1,
            'rowCount' => count($documents),
            'rows' => DTOSerializer::serialize($documents),
            'total' => count($documents),
        ]);
    }

    public function itemAction(Request $request, string $id): JsonResponse
    {
        if (!$document = $this->documentRepository->documentOfId(DocumentId::fromString($id))) {
            return new JsonResponse(null, Response::HTTP_NOT_FOUND);
        }

        return new JsonResponse(DTOSerializer::serialize($document));
    }

    public function contentAction(Request $request, string $id): Response
    {
        if (!$document = $this->documentRepository->get(DocumentId::fromString($id))) {
            return new Response('', Response::HTTP_NOT_FOUND);
        }

        $content = is_resource($document->data()) ? stream_get_contents($document->data()) : $document->data();

        return new Response($content, Response::HTTP_OK, ['Content-Type' => (string)$document->mimeType()]);
    }

    public function createAction(Request $request): JsonResponse
    {
        /** @var UploadedFile $uploadedFile */
        $uploadedFile = $request->files->get('0');

        try {
            $document = $this->documentService->createDocument(new CreateDocument($uploadedFile));
        } catch (\Exception $e) {
            return JsonResponse::create(['error' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }

        return new JsonResponse(['id' => $document->documentId()], Response::HTTP_CREATED);
    }

    public function removeAction(Request $request, string $id): JsonResponse
    {
        try {
            $this->documentService->removeDocument(new RemoveDocument(DocumentId::fromString($id)));
        } catch (\Exception $e) {
            return JsonResponse::create(['error' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }

        return new JsonResponse(null, Response::HTTP_ACCEPTED);
    }
}
