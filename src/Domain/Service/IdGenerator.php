<?php

namespace Prima\CMS\Domain\Service;

use Prima\CMS\Domain\Model\AggregateId;

interface IdGenerator
{
    /**
     * @return AggregateId UUID
     */
    public function next();
}
