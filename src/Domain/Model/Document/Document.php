<?php

namespace Prima\CMS\Domain\Model\Document;

class Document
{
    /**
     * @var DocumentId
     */
    private $documentId;
    /**
     * @var DocumentName
     */
    private $name;
    /**
     * @var MimeType
     */
    private $mimeType;
    /**
     * @var mixed
     */
    private $data;

    private function __construct(DocumentId $id, DocumentName $name, MimeType $mimeType, $data)
    {
        $this->documentId = $id;
        $this->name = $name;
        $this->mimeType = $mimeType;
        $this->data = $data;
    }

    public static function create(DocumentId $documentId, DocumentName $name, MimeType $mimeType, $data)
    {
        return new self($documentId, $name, $mimeType, $data);
    }

    public function documentId(): DocumentId
    {
        return $this->documentId;
    }

    public function name(): DocumentName
    {
        return $this->name;
    }

    public function mimeType(): MimeType
    {
        return $this->mimeType;
    }

    public function data()
    {
        return $this->data;
    }
}
