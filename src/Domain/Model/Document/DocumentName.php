<?php

namespace Prima\CMS\Domain\Model\Document;

use Assert\Assertion;
use Prima\CMS\Domain\Model\ValueObject;

class DocumentName implements ValueObject
{
    const MIN_LENGTH = 5;
    const MAX_LENGTH = 100;

    /** @var  string */
    private $name;

    private function __construct(string $name)
    {
        $this->setName($name);
    }

    public static function fromString(string $name): DocumentName
    {
        return new self($name);
    }

    public function name(): string
    {
        return $this->name;
    }

    public function __toString(): string
    {
        return $this->name;
    }

    private function setName(string $name)
    {
        Assertion::notBlank(trim($name));
        Assertion::betweenLength($name, self::MIN_LENGTH, self::MAX_LENGTH);

        $this->name = $name;
    }
}
