<?php

namespace Prima\CMS\Domain\Model\Document;

use InvalidArgumentException;
use Prima\CMS\Domain\Model\Enum;

final class MimeType extends Enum
{
    const PDF = 'application/pdf';
    const WORD = 'application/msword';

    /**
     * @param mixed $value
     * @throws \InvalidArgumentException
     */
    protected function guardIsValid($value)
    {
        if (!static::isValid($value)) {
            throw new InvalidArgumentException(sprintf('%s is not supported', $value));
        }
    }
}
