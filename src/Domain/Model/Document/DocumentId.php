<?php

namespace Prima\CMS\Domain\Model\Document;

use Assert\Assertion;
use Prima\CMS\Domain\Model\AggregateId;
use Prima\CMS\Domain\Model\Equatable;

class DocumentId implements AggregateId, Equatable
{
    /** @var  string */
    private $id;

    private function __construct(string $id)
    {
        $this->setId($id);
    }

    public static function fromString(string $id): DocumentId
    {
        return new self($id);
    }

    public function id(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @throws \Assert\AssertionFailedException
     */
    private function setId(string $id)
    {
        Assertion::notBlank(trim($id));
        Assertion::uuid($id);

        $this->id = $id;
    }

    public function __toString(): string
    {
        return $this->id;
    }

    /**
     * @param mixed $object
     * @return bool
     */
    public function equals($object): bool
    {
        return (($object instanceof self) && $this->id === $object->id);
    }
}
