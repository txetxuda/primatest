<?php

namespace Prima\CMS\Domain\Model\Document;

use Prima\CMS\Domain\Model\DTO;

class DocumentDTO implements DTO
{
    /**
     * @var string
     */
    private $id;
    /**
     * @var string
     */
    private $name;
    /**
     * @var string
     */
    private $mimeType;
    /**
     * @var string
     */
    private $url;

    public function __construct(string $id, string $name, string $mimeType, string $url)
    {
        $this->id = $id;
        $this->name = $name;
        $this->mimeType = $mimeType;
        $this->url = $url;
    }

    public function id(): string
    {
        return $this->id;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function mimeType(): string
    {
        return $this->mimeType;
    }

    public function url(): string
    {
        return $this->url;
    }

    public function serialize(): array
    {
        return [
            'id' => $this->id(),
            'name' => $this->name(),
            'mime_type' => $this->mimeType(),
            'url' => $this->url(),
        ];
    }

    public static function fromArray(array $data): DTO
    {
        return new self($data['id'], $data['name'], $data['mime_type'], $data['url']);
    }
}
