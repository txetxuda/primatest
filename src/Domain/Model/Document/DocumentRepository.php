<?php

namespace Prima\CMS\Domain\Model\Document;

interface DocumentRepository
{
    /**
     * @param DocumentId $documentId
     * @return null|Document
     */
    public function get(DocumentId $documentId);
    /**
     * @return DocumentDTO[]
     */
    public function all();

    /**
     * @param DocumentId $documentId
     * @return null|DocumentDTO
     */
    public function documentOfId(DocumentId $documentId);

    public function save(Document $document);

    public function remove(Document $document);
}
