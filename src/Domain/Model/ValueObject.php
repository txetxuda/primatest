<?php

namespace Prima\CMS\Domain\Model;

interface ValueObject
{
    /**
     * @param string $id
     * @return ValueObject
     */
    public static function fromString(string $id);

    public function __toString(): string;
}
