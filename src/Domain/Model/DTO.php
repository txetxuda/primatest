<?php

namespace Prima\CMS\Domain\Model;

interface DTO
{
    /**
     * @return array
     */
    public function serialize(): array;

    /**
     * @param array $data
     * @return DTO
     */
    public static function fromArray(array $data): DTO;
}
