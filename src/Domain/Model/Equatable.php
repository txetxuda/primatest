<?php

namespace Prima\CMS\Domain\Model;

interface Equatable
{
    /**
     * @param mixed $object
     * @return bool
     */
    public function equals($object): bool;
}
