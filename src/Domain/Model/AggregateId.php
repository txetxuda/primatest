<?php

namespace Prima\CMS\Domain\Model;

interface AggregateId
{
    /**
     * @param string $id
     * @return AggregateId
     */
    public static function fromString(string $id);

    public function __toString(): string;
}
