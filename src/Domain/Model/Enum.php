<?php

namespace Prima\CMS\Domain\Model;

use InvalidArgumentException;
use ReflectionClass;

abstract class Enum implements Equatable
{
    /** @var array  */
    protected static $classConstantsCache = [];
    /** @var mixed */
    protected $value;

    /** @param mixed $value */
    public function __construct($value)
    {
        $this->guardIsValid($value);
        $this->value = $value;
    }

    /**
     * @param string $name
     * @param mixed  $arguments
     *
     * @return static
     */
    public static function __callStatic($name, $arguments)
    {
        $snake = (strtolower($name) == $name) ? $name : strtolower(preg_replace('/([^A-Z\s])([A-Z])/', "$1_$2", $name));

        return new static($snake);
    }

    /**
     * @param string $value
     *
     * @return static
     */
    public static function fromString($value)
    {
        return new static($value);
    }

    /**
     * @return array
     */
    public static function validValues(): array
    {
        $class = get_called_class();

        if (!isset(static::$classConstantsCache[$class])) {
            $reflected = new ReflectionClass($class);
            static::$classConstantsCache[$class] = array_values($reflected->getConstants());
        }

        return static::$classConstantsCache[$class];
    }

    /**
     * @return static
     */
    public static function randomValue(): self
    {
        return static::validValues()[array_rand(static::validValues())];
    }

    /**
     * @return static
     */
    public static function random(): self
    {
        return new static(static::randomValue());
    }

    /**
     * @return mixed
     */
    public function value()
    {
        return $this->value;
    }

    /**
     * @param mixed $object
     *
     * @return bool
     */
    public function equals($object): bool
    {
        return $object == $this;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return (string) $this->value();
    }

    /**
     * @param mixed $value
     */
    protected function guardIsValid($value)
    {
        if (!static::isValid($value)) {
            $this->throwIsInvalid($value);
        }
    }

    /**
     * @param mixed $value
     *
     * @return bool
     */
    protected static function isValid($value): bool
    {
        return in_array($value, static::validValues(), true);
    }

    /**
     * @param mixed $value
     * @throws InvalidArgumentException
     */
    protected function throwIsInvalid($value)
    {
        throw new InvalidArgumentException(sprintf('%s <%s> is invalid', get_class($this), $value));
    }
}
