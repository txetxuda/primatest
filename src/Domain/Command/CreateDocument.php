<?php

namespace Prima\CMS\Domain\Command;

use Symfony\Component\HttpFoundation\File\UploadedFile;

class CreateDocument
{
    /**
     * @var UploadedFile
     */
    private $uploadedFile;

    public function __construct(UploadedFile $uploadedFile)
    {
        $this->uploadedFile = $uploadedFile;
    }

    public function uploadedFile(): UploadedFile
    {
        return $this->uploadedFile;
    }
}
