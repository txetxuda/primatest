<?php

namespace Prima\CMS\Domain\Command;

use Prima\CMS\Domain\Model\Document\DocumentId;

class RemoveDocument
{
    /**
     * @var DocumentId
     */
    private $documentId;

    public function __construct(DocumentId $documentId)
    {
        $this->documentId = $documentId;
    }

    public function documentId(): DocumentId
    {
        return $this->documentId;
    }
}
