<?php

namespace Prima\CMS\Domain\Exception;

class DocumentNotFoundException extends EntityNotFoundException
{
}
