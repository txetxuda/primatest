<?php

namespace Prima\CMS\Domain\Exception;

class EntityNotFoundException extends \Exception
{
}
