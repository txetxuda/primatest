#!/bin/sh

echo "Starting containers...."
docker-compose up -d

echo "Configuring folder permissions...."
docker exec -itu `id -u $USER`:`id -g $USER` primatest_php_1 mkdir /prima-cms/var/sessions
docker exec -itu `id -u $USER`:`id -g $USER` primatest_php_1 mkdir /prima-cms/var/cache
docker exec -itu `id -u $USER`:`id -g $USER` primatest_php_1 mkdir /prima-cms/var/logs
docker exec -itu `id -u $USER`:`id -g $USER` primatest_php_1 chmod -R 777 /prima-cms/var/sessions
docker exec -itu `id -u $USER`:`id -g $USER` primatest_php_1 chmod -R 777 /prima-cms/var/cache
docker exec -itu `id -u $USER`:`id -g $USER` primatest_php_1 chmod -R 777 /prima-cms/var/logs

echo "Installing PHP dependencies....."
docker run -u `id -u $USER`:`id -g $USER` --rm -v $(pwd)/:/app -v ~/.ssh:/root/.ssh composer/composer install

echo "Performing database setup....."
docker exec -itu `id -u $USER`:`id -g $USER` primatest_php_1 /prima-cms/bin/console --no-interaction doctrine:migrations:migrate

echo "Running Tests...."
docker exec -itu `id -u $USER`:`id -g $USER` primatest_php_1 /prima-cms/vendor/bin/phpunit -c /prima-cms/phpunit.xml.dist

echo "Installing Javascript dependencies"
docker run -u `id -u $USER`:`id -g $USER` --rm -v $(pwd)/web:/prima-cms/web -w /prima-cms/web node:4 npm install
