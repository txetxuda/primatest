# Nginx configuration

server {
    listen 80 default_server;
    listen [::]:80 default_server;
    server_name localhost;

    error_log  /var/log/nginx/error.log;
    access_log /var/log/nginx/access.log;
    root /prima-cms/web;

    # strip app.php/ prefix if it is present
    rewrite ^/app\.php/?(.*)$ /$1 permanent;
    client_max_body_size 20M;

    location / {
        index index.html;
        try_files $uri $uri/ @rewriteapp;

        # pass the PHP scripts to php-fpm
        location ~ \.php(/|$) {
           fastcgi_pass php:9000;
           fastcgi_split_path_info ^(.+\.php)(/.*)$;
           include fastcgi_params;
           fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
           fastcgi_param PATH_INFO $fastcgi_path_info;
       }
    }

    location @rewriteapp {
        rewrite ^(.*)$ /app.php$1 last;
    }
}
