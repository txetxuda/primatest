##
PDF Manager
=========== 
##

### **Steps to run the project** ###
**In order to run the project you need to have docker and docker-compose properly installed in your machine**

**1.** Clone the project

```
#!shell

git clone git@bitbucket.org:txetxuda/primatest.git
```

**2.** Run installation script:

```
#!shell

cd primatest
bin/install.sh
```

**3.** Open **http://172.20.0.20/documents** and enjoy!