<?php

namespace Test\Prima\CMS\Application\Service\Document;

use PHPUnit\Framework\TestCase;
use Prima\CMS\Application\Service\Document\URLGenerator;
use Prima\CMS\Domain\Model\Document\Document;
use Prima\CMS\Domain\Model\Document\DocumentId;
use Prima\CMS\Domain\Model\Document\DocumentName;
use Prima\CMS\Domain\Model\Document\MimeType;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;

class URLGeneratorTest extends TestCase
{
    /** @var  RouterInterface */
    private $router;
    /** @var  URLGenerator */
    private $urlGenerator;

    public function setUp()
    {
        $this->router = $this->prophesize(RouterInterface::class);

        $this->urlGenerator = new URLGenerator($this->router->reveal());
    }

    /**
     * @test
     */
    public function it_should_create_document_url()
    {
        $id = Uuid::uuid4();
        $documentId = DocumentId::fromString($id);
        $name = DocumentName::fromString('some name');
        $document = Document::create($documentId, $name, MimeType::fromString(MimeType::PDF), '');

        $this->router->generate('document_content', ['id' => $documentId], UrlGeneratorInterface::ABSOLUTE_URL)
            ->shouldBeCalled()
            ->willReturn('http://www.pdf-manager.com/documents/'.$documentId.'/file');

        $actual = $this->urlGenerator->generate($document);

        static::assertEquals('http://www.pdf-manager.com/documents/'.(string) $documentId.'/file', $actual);
    }
}
