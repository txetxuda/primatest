<?php

namespace Test\Prima\CMS\Application\Service\Document;

use PHPUnit\Framework\TestCase;
use Prima\CMS\Application\Service\Document\DocumentDTOHydrator;
use Prima\CMS\Application\Service\Document\URLGenerator;
use Prima\CMS\Domain\Model\Document\Document;
use Prima\CMS\Domain\Model\Document\DocumentId;
use Prima\CMS\Domain\Model\Document\DocumentName;
use Prima\CMS\Domain\Model\Document\MimeType;
use Ramsey\Uuid\Uuid;

class DocumentDTOHydratorTest extends TestCase
{
    /** @var  URLGenerator */
    private $urlGenerator;
    /** @var  DocumentDTOHydrator */
    private $documentDTOHydrator;

    public function setUp()
    {
        $this->urlGenerator = $this->prophesize(URLGenerator::class);
        $this->documentDTOHydrator = new DocumentDTOHydrator($this->urlGenerator->reveal());
    }

    /**
     * @test
     */
    public function it_should_hydrate_document_to_dto()
    {
        $documentId = DocumentId::fromString(Uuid::uuid4());
        $name = DocumentName::fromString('document1');
        $mimeType = MimeType::fromString(MimeType::PDF);
        $document = Document::create($documentId, $name, $mimeType, 'some blob data');

        $this->urlGenerator->generate($document)->willReturn('some url');
        $actual = $this->documentDTOHydrator->hydrate($document);

        static::assertEquals((string)$documentId, $actual->id());
        static::assertEquals('some url', $actual->url());
        static::assertEquals((string) $name, $actual->name());
        static::assertEquals((string) $mimeType, $actual->mimeType());
    }
}
