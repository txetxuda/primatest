<?php

namespace Test\Prima\CMS\Application\Service\Document;

use PHPUnit\Framework\TestCase;
use Prima\CMS\Application\Service\Document\DocumentService;
use Prima\CMS\Domain\Command\CreateDocument;
use Prima\CMS\Domain\Command\RemoveDocument;
use Prima\CMS\Domain\Exception\DocumentNotFoundException;
use Prima\CMS\Domain\Model\Document\Document;
use Prima\CMS\Domain\Model\Document\DocumentId;
use Prima\CMS\Domain\Model\Document\DocumentName;
use Prima\CMS\Domain\Model\Document\DocumentRepository;
use Prima\CMS\Domain\Model\Document\MimeType;
use Prima\CMS\Domain\Service\IdGenerator;
use Prophecy\Argument;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class DocumentServiceTest extends TestCase
{
    /** @var  IdGenerator */
    private $idGenerator;
    /** @var  DocumentService */
    private $documentService;
    /** @var  DocumentRepository */
    private $documentRepository;

    public function setUp()
    {
        $this->documentRepository = $this->prophesize(DocumentRepository::class);
        $this->idGenerator = $this->prophesize(IdGenerator::class);

        $this->documentService = new DocumentService($this->documentRepository->reveal(), $this->idGenerator->reveal());
    }

    /**
     * @test
     */
    public function it_should_remove_existing_document()
    {
        $id = Uuid::uuid4();
        $documentId = DocumentId::fromString($id);
        $name = DocumentName::fromString('some name');
        $document = Document::create($documentId, $name, MimeType::fromString(MimeType::PDF), '');

        $this->documentRepository->get($documentId)->willReturn($document);
        $this->documentRepository->remove($document)->shouldBeCalled();

        $this->documentService->removeDocument(new RemoveDocument($documentId));
    }

    /**
     * @test
     */
    public function it_should_throw_exception_when_document_not_found_removing_document()
    {
        $id = Uuid::uuid4();
        $documentId = DocumentId::fromString($id);
        $name = DocumentName::fromString('some name');
        $document = Document::create($documentId, $name, MimeType::fromString(MimeType::PDF), '');

        $this->documentRepository->get($documentId)->willReturn(null);
        $this->documentRepository->remove($document)->shouldNotBeCalled();

        $this->expectException(DocumentNotFoundException::class);

        $this->documentService->removeDocument(new RemoveDocument($documentId));
    }

    /**
     * @test
     */
    public function it_should_create_new_document()
    {
        $uuid = Uuid::uuid4();
        $fileName = 'some_document_name';
        $pdf = new UploadedFile(__DIR__.'/../../../fixtures/sample.pdf', $fileName, MimeType::PDF);

        $this->idGenerator->next()->willReturn($uuid);
        $this->documentRepository->save(Argument::type(Document::class))->shouldBeCalled();

        $actual = $this->documentService->createDocument(new CreateDocument($pdf));

        static::assertInstanceOf(Document::class, $actual);
        static::assertEquals($uuid, (string) $actual->documentId());
        static::assertEquals($fileName, $actual->name());
    }
}
