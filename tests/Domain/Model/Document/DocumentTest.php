<?php

namespace Test\Prima\CMS\Domain\Model\Document;

use PHPUnit\Framework\TestCase;
use Prima\CMS\Domain\Model\Document\Document;
use Prima\CMS\Domain\Model\Document\DocumentId;
use Prima\CMS\Domain\Model\Document\DocumentName;
use Prima\CMS\Domain\Model\Document\MimeType;
use Ramsey\Uuid\Uuid;

class DocumentTest extends TestCase
{
    /**
     * @test
     */
    public function it_should_create_document()
    {
        $documentId = DocumentId::fromString(Uuid::uuid4());
        $name = DocumentName::fromString('some name');
        $mimeType = MimeType::fromString(MimeType::PDF);
        $data = 'some blob data';
        $document = Document::create($documentId, $name, $mimeType, $data);

        static::assertInstanceOf(Document::class, $document);
        static::assertEquals($documentId, $document->documentId());
        static::assertEquals($name, $document->name());
        static::assertEquals($mimeType, $document->mimeType());
        static::assertEquals($data, $document->data());
    }
}
