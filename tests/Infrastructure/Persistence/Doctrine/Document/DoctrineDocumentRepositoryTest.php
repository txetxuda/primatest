<?php

namespace Test\Prima\CMS\Infrastructure\Persistence\Doctrine\Document;

use Doctrine\ORM\EntityManagerInterface;
use Prima\CMS\Domain\Model\Document\Document;
use Prima\CMS\Domain\Model\Document\DocumentId;
use Prima\CMS\Domain\Model\Document\DocumentName;
use Prima\CMS\Domain\Model\Document\MimeType;
use Prima\CMS\Infrastructure\Persistence\Doctrine\Document\DoctrineDocumentRepository;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class DoctrineDocumentRepositoryTest extends KernelTestCase
{
    /** @var  DoctrineDocumentRepository */
    private $documentRepository;
    /** @var  EntityManagerInterface */
    private $em;

    public function setUp()
    {
        self::bootKernel();
        $this->em = static::$kernel->getContainer()->get('doctrine')->getManager();
        $documentDTOHydrator = static::$kernel->getContainer()->get('prima.cms.document.dto_hydrator');

        $this->documentRepository = $this->em->getRepository(Document::class);
        $this->documentRepository->setHydrator($documentDTOHydrator);
    }

    /**
     * @test
     */
    public function it_should_persist_new_document()
    {
        $documentId = DocumentId::fromString(Uuid::uuid4());
        $name = DocumentName::fromString('some name');
        $mimeType = MimeType::fromString(MimeType::PDF);
        $document = Document::create($documentId, $name, $mimeType, 'some blob data');

        $this->documentRepository->save($document);

        static::assertEquals($document, $this->documentRepository->get($documentId));
    }

    /**
     * @test
     */
    public function it_should_remove_document()
    {
        $documentId = DocumentId::fromString(Uuid::uuid4());
        $name = DocumentName::fromString('some name');
        $mimeType = MimeType::fromString(MimeType::PDF);
        $document = Document::create($documentId, $name, $mimeType, 'some blob data');

        $this->documentRepository->save($document);

        static::assertEquals($document, $this->documentRepository->get($documentId));

        $this->documentRepository->remove($document);

        static::assertNull($this->documentRepository->get($documentId));
    }

    /**
     * @test
     */
    public function it_should_get_all_available_documents()
    {
        $documentId1 = DocumentId::fromString(Uuid::uuid4());
        $name1 = DocumentName::fromString('document1');
        $mimeType1 = MimeType::fromString(MimeType::PDF);
        $document1 = Document::create($documentId1, $name1, $mimeType1, 'some blob data');
        $this->documentRepository->save($document1);

        $documentId2 = DocumentId::fromString(Uuid::uuid4());
        $name2 = DocumentName::fromString('document2');
        $mimeType2 = MimeType::fromString(MimeType::PDF);
        $document2 = Document::create($documentId2, $name2, $mimeType2, 'some blob data');
        $this->documentRepository->save($document2);

        $documents = $this->documentRepository->all();

        static::assertCount(2, $documents);
    }

    protected function tearDown()
    {
        parent::tearDown();

        $conn = $this->em->getConnection();
        $conn->executeQuery('TRUNCATE documents');
        $this->em->close();
    }
}
