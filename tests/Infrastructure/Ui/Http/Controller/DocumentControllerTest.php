<?php

namespace Test\Prima\CMS\Infrastructure\Ui\Http\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Prima\CMS\Domain\Command\CreateDocument;
use Prima\CMS\Domain\Model\Document\Document;
use Prima\CMS\Domain\Model\Document\DocumentId;
use Prima\CMS\Domain\Model\Document\DocumentName;
use Prima\CMS\Domain\Model\Document\DocumentRepository;
use Prima\CMS\Domain\Model\Document\MimeType;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class DocumentControllerTest extends WebTestCase
{
    /** @var  EntityManagerInterface */
    private $entityManager;
    /** @var  DocumentRepository */
    private $documentRepository;
    /** @var  Client */
    private $client;

    public function setUp()
    {
        $this->client = static::createClient();
        $this->documentRepository = $this->client->getContainer()->get('prima.cms.document.repository');
        $this->entityManager = $this->client->getContainer()->get('doctrine')->getManager();
    }

    /**
     * @test
     */
    public function it_should_get_all_documents()
    {
        $documentId = DocumentId::fromString(Uuid::uuid4());
        $name = DocumentName::fromString('document1');
        $mimeType = MimeType::fromString(MimeType::PDF);
        $document = Document::create($documentId, $name, $mimeType, 'some blob data');
        $this->createDocumentFixture($document);

        $this->client->request('GET', '/api/v1/documents');
        $response = $this->client->getResponse();

        $expectedResponse = [
            'current' => 1,
            'rowCount' => 1,
            'rows' => [
                [
                    'id' => (string) $documentId,
                    'name' => (string) $name,
                    'mime_type' => (string) $mimeType,
                    'url' => 'http://localhost/api/v1/documents/'.$documentId.'/file',

                ],
            ],
            'total' => 1,
        ];

        $actual = json_decode($response->getContent(), true);

        static::assertSame(200, $response->getStatusCode());
        static::assertEquals($expectedResponse, $actual);
    }

    /**
     * @test
     */
    public function it_should_get_document_info()
    {
        $documentId = DocumentId::fromString(Uuid::uuid4());
        $name = DocumentName::fromString('document1');
        $mimeType = MimeType::fromString(MimeType::PDF);
        $document = Document::create($documentId, $name, $mimeType, 'some blob data');
        $this->createDocumentFixture($document);

        $this->client->request('GET', '/api/v1/documents/'.$documentId);
        $response = $this->client->getResponse();

        $expectedResponse = [
            'id' => (string) $documentId,
            'name' => (string) $name,
            'mime_type' => (string) $mimeType,
            'url' => 'http://localhost/api/v1/documents/'.$documentId.'/file',
        ];

        static::assertSame(200, $response->getStatusCode());
        static::assertEquals($expectedResponse, json_decode($response->getContent(), true));
    }

    /**
     * @test
     */
    public function it_should_get_document_content()
    {
        $rootPath = $this->client->getContainer()->getParameter('kernel.root_dir');
        $pdf = new UploadedFile($rootPath.'/../tests/fixtures/sample.pdf', 'sample.pdf', MimeType::PDF);

        $documentService = $this->client->getContainer()->get('prima.cms.document.service');
        $document = $documentService->createDocument(new CreateDocument($pdf));

        $this->client->request('GET', '/api/v1/documents/'.$document->documentId().'/file');
        $response = $this->client->getResponse();

        static::assertSame(200, $response->getStatusCode());
        static::assertEquals($document->mimeType(), $response->headers->get('content-type'));
    }

    /**
     * @test
     */
    public function it_should_create_new_document()
    {
        $rootPath = $this->client->getContainer()->getParameter('kernel.root_dir');
        $pdf = new UploadedFile($rootPath.'/../tests/fixtures/sample.pdf', 'sample.pdf', MimeType::PDF);

        $this->client->request(
            'POST',
            '/api/v1/documents',
            [],
            ['0' => $pdf]
        );

        $response = $this->client->getResponse();

        static::assertSame(201, $response->getStatusCode());

        $this->client->request('GET', '/api/v1/documents');
        $response = $this->client->getResponse();

        static::assertSame(200, $response->getStatusCode());
    }

    /**
     * @test
     */
    public function it_should_remove_document()
    {
        $documentId = DocumentId::fromString(Uuid::uuid4());
        $name = DocumentName::fromString('document1');
        $mimeType = MimeType::fromString(MimeType::PDF);
        $document = Document::create($documentId, $name, $mimeType, 'some blob data');
        $this->createDocumentFixture($document);

        $this->client->request('GET', '/api/v1/documents/'.$documentId);
        $response = $this->client->getResponse();

        $expectedResponse = [
            'id' => (string) $documentId,
            'name' => (string) $name,
            'mime_type' => (string) $mimeType,
            'url' => 'http://localhost/api/v1/documents/'.$documentId.'/file',
        ];

        static::assertSame(200, $response->getStatusCode());
        static::assertEquals($expectedResponse, json_decode($response->getContent(), true));

        $this->client->request('DELETE', '/api/v1/documents/'.$documentId);
        $response = $this->client->getResponse();

        static::assertSame(202, $response->getStatusCode());

        $this->client->request('GET', '/api/v1/documents/'.$documentId);
        $response = $this->client->getResponse();

        static::assertSame(404, $response->getStatusCode());
    }

    private function createDocumentFixture(Document $document)
    {
        $this->documentRepository->save($document);
    }

    protected function tearDown()
    {
        parent::tearDown();

        $conn = $this->entityManager->getConnection();
        $conn->executeQuery('TRUNCATE documents');
        $this->entityManager->close();
    }
}
