<?php

namespace Test\Prima\CMS\Infrastructure\Service;

use Assert\Assertion;
use Prima\CMS\Infrastructure\Service\DocumentIdGenerator;

class DocumentIdGeneratorTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function it_should_create_next_document_id()
    {
        $documentIdGenerator = new DocumentIdGenerator();
        $actual = $documentIdGenerator->next();

        Assertion::uuid($actual);
    }
}
